<?php

namespace Nrn\Nrnframework;

class Response
{
    public function setStatusCode($code)
    {
        http_response_code($code);
    }

}