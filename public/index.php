<?php

use Nrn\Nrnframework\Application;

require_once '../vendor/autoload.php';

$app = new Application();

require '../routes/web.php';

$app->run();