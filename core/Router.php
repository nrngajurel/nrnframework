<?php

namespace Nrn\Nrnframework;

class Router
{
    private Request $request;
    private Response $response;

    public array $routes =[
        'get' => [],
        'post' => []
    ];


    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function get($path, $callback){
        $this->routes['get'][strtolower($path)] = $callback;
    }

    public function post($path, $callback){
        $this->routes['post'][$path] = $callback;
    }

    /**
     * @throws \Exception
     */
    public function resolve(){
        $path = $this->request->getPath();
        $method = $this->request->getMethod();
        $callback = $this->routes[$method][$path] ?? false;
        if(!$callback){
            $this->response->setStatusCode(404);
            return view()->render('errors/404');
//            throw new \Exception('No route found');
        }
        if (is_string($callback)) {
            return view()->render($callback);
        }
        if(is_array($callback)){
            $controller  = new $callback[0];
            $callback[0] = $controller;
        }
        return call_user_func($callback, $this->request, $this->response);
    }
}