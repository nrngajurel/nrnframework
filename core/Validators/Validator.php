<?php

namespace Nrn\Nrnframework\Validators;

interface Validator
{
    public function validate($value, $key, $parameters = null);

    public function getMessage();
}