<?php

namespace Nrn\Nrnframework;

class Config
{
    public static function get($key = null){
        $a = explode('.', $key);
        $data =[];
        $file = configDir($a[0].".php");
        if (file_exists($file) ){
            $data = include configDir($a[0].".php");
        }
        return $data;

    }


}