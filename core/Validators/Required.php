<?php

namespace Nrn\Nrnframework\Validators;

class Required implements Validator
{
    private $value;
    private $key;
    public function validate($value, $key , $parameters = null)
    {
        $this->value = $value;
        $this->key = $key;
        return !empty($value);
    }

    public function getMessage()
    {
        return "The field $this->key is required";
    }
}