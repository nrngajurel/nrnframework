<?php

namespace Nrn\Nrnframework;

use Nrn\Nrnframework\Exceptions\ValidationException;
use Nrn\Nrnframework\Validators\Required;

class Request
{
    public function getPath(){
        $path = $_SERVER['REQUEST_URI']??'/';
        if(str_contains($path, '?')){
            $path = substr($path, 0, strpos($path, '?'));
        }
        return strtolower($path);
    }

    public function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function all(){
        $data = [];
        if ($this->getMethod() == 'get') {
            foreach ($_GET as $key => $value) {
                $data[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        } elseif ($this->getMethod() == 'post') {
            foreach ($_POST as $key => $value) {
                $data[$key] = filter_input(INPUT_POST,$key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        return $data;
    }
    public function get($key){
        return $this->all()[$key]??null;
    }
    public function validate($data, $rules){
        $validators = [
            'required' => Required::class
        ];
        $errors  = [];
        $proceedData = [];
        foreach ($rules as $key => $rule) {
            $rules[$key] = explode('|', $rule);

            foreach ($rules[$key] as $rule){
                    if(array_key_exists($rule ,$validators)){
                        $validator = new $validators[$rule];

                        if(!$validator->validate($data[$key], $key)){
                            $errors[$key] = $validator->getMessage();
                        }else{
                            $proceedData[$key] = $data[$key];
                        }
                    }
                }
            };
        if(count($errors) > 0){
            throw new ValidationException("Validation failed", $errors);
        }
        return $proceedData;
    }
}