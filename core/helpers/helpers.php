<?php

require_once 'app.php';
use JetBrains\PhpStorm\NoReturn;

#[NoReturn] function dd($data){
    echo '<pre>';
    print_r(func_get_args());
    echo '</pre>';
    die();
}

function projDir(): string
{
    return $_SERVER['DOCUMENT_ROOT'] . '/../';
//    return __DIR__ . '/../../';
}
function configDir($file_name = ''): string
{
    return projDir() . 'config/'. $file_name;
}