<?php

namespace Nrn\Nrnframework;

class Controller
{
    public function view(string $view, array $data = [])
    {
        return view()->render($view, $data);
    }



}