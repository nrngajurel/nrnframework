<?php

use Nrn\Nrnframework\Application;
use Nrn\Nrnframework\Request;
use Nrn\Nrnframework\Response;
use Nrn\Nrnframework\Router;
use Nrn\Nrnframework\Session;
use Nrn\Nrnframework\View;


function app(): Application
{
    return Application::$app;
}
function router(): Router
{
    return Application::$app->router;
}

function request(): Request
{
    return Application::$app->request;
}

function response(): Response
{
    return Application::$app->response;
}

function view(): View
{
    return Application::$app->view;
}

function viewRender(string $view, array $data = []): array|string
{
    return Application::$app->view->render($view, $data);
}

function session(): Session
{
    return Application::$app->session;
}
function config(string $key): array
{
    return Application::$app->config::get($key);
}