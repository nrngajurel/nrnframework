<?php

namespace App\Controllers;

use Nrn\Nrnframework\Controller;
use Nrn\Nrnframework\Request;

class SiteController extends Controller
{

    public function home(Request $request){
        $data = [
          'title' => 'Home',
        ];

        return $this->view('home', $data);
    }

}