<?php

use App\Controllers\SiteController;

router()->get('/', [SiteController::class, 'home']);
router()->get('/contact', 'contact');
router()->get('/login',[\App\Controllers\AuthController::class,'login']);
router()->get('/register',[\App\Controllers\AuthController::class,'register']);
router()->post('/login',[\App\Controllers\AuthController::class,'handleLogin']);
router()->post('/register',[\App\Controllers\AuthController::class,'handleRegister']);