<?php

namespace App\Controllers;

use App\Models\User;
use Nrn\Nrnframework\Controller;
use Nrn\Nrnframework\Exceptions\ValidationException;
use Nrn\Nrnframework\Request;

class AuthController extends Controller
{
    public function login()
    {
        view()->setLayout('auth');
        return $this->view('auth/login');
    }

    public function register()
    {
        view()->setLayout('auth');
        return $this->view('auth/register');
    }
    public function handleLogin(Request $request){
        try {
            $data = $request->validate($request->all(), [
                'email' => 'required',
                'password' => 'required'
            ]);
        }catch (ValidationException $e){
            dd($e->getErrors());
        }
        $user = new User();
        $user->create($data);

    }
}