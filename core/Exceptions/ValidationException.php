<?php

namespace Nrn\Nrnframework\Exceptions;

class ValidationException extends Exception
{
    /**
     * @var array
     */
    private $errors;

    /**
     * @param string $message
     * @param array $errors
     */
    public function __construct($message, array $errors)
    {
        parent::__construct($message);
        $this->errors = $errors;
        $this->code = 422;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}