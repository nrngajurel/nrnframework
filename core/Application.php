<?php

namespace Nrn\Nrnframework;

class Application
{
    public static Application $app;
    public Request $request;
    public Response $response;
    public View $view;
    public Session $session;
    public Database $database;
    public Config $config;
    public Router $router;
    public function __construct()
    {
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->view = new View();
        $this->session = new Session();
        $this->database = new Database();
        $this->config = new Config();
        $this->router = new Router($this->request, $this->response);
    }


    /**
     * @throws \Exception
     */
    public function run()
    {
        echo router()->resolve();
    }

}