<?php

namespace Nrn\Nrnframework;

class View
{
    public $layout = 'main';
    public function __construct()
    {
    }

    public function setLayout($layout){
        $this->layout = $layout;
    }
    public function render($path, $data = []){

        $layout = $this->layout($this->layout, $data);
        $content = $this->renderOnlyView($path, $data);
        if (!$this->layout){
            return $content;
        }
        return str_replace('{{slot}}', $content, $layout)??$content;
    }
    public function renderContent($viewContent, $data = []){

        $layout = $this->layout($this->layout, $data);
        return str_replace('{{slot}}', $viewContent, $layout);
    }

    public function layout($path, $data = []){
        if ($path == null || $path == '') {
            return '';
        }
        return $this->renderOnlyView('layouts/'.$path, $data);
    }
    public function renderOnlyView($path, $data = []){
        foreach ($data as $key => $value) {
            $$key = $value;
        }
        ob_start();
        $path = config('views')['path'].'/'.$path.'.php';
        if(file_exists($path)){
            include_once $path;
        }
        else{
            echo 'View not found<br/>';
            echo str_replace(config('views')['path'].'/', '', $path);
        }
        return ob_get_clean();
    }

}